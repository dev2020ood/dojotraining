define([
    "dojo/_base/declare",
    "dojo/on",
    "dijit/_WidgetBase",
    "../boardService/boardService.js",
    "dijit/_TemplatedMixin",
    "dojo/text!/Widget/templates/Widget.html"
], function(declare, on, _WidgetBase,Board, _TemplatedMixin, template){
    return declare([_WidgetBase, _TemplatedMixin], {
        Cell:"unknowm",
        Names:"unknown",
        _setCellAttr:{node:"cellNode",type:"innerHTML"},
        templateString: template,
        postCreate:function(){
            
           
            Board.Foo()
            Board.Inc()
            Board.Foo()
          
            on(this.btnNode,"click",evt=> this.set("Cell","X"))
            console.log(this.txtFilterNode)
            on(this.txtFilterNode,"keyup",evt=>this.startTimer())
            
            
        },
        render:function(){

        },
        startTimer:function (){
                console.log("Start Timer")
        }

        
    });
});